const changeLessonType = (state) => {
  return {
    ...state,
    online: !state.online
  };
};

export default changeLessonType;