import uuid from 'uuid/v4';

const selectStudent = (state, action) => {
  return [
    ...state,
    {
      ...action.obj,
      id: uuid()
    },
  ];
};

export default selectStudent;