const selectStudent = (state, action) => {
  return {
    ...state,
    student: {
      ...action.obj
    }
  };
};

export default selectStudent;