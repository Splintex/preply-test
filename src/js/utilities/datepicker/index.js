import rome from 'rome';
import moment from 'moment';

class RomeDatePicker {
  constructor(selector, options) {
    this.selector = selector;
    this.datePickers = document.querySelectorAll(selector);
    this.format = {
      year: 'YYYY',
      date: 'MMMM D',
      day: 'dddd'
    };
    this.options = {
      time: false,
      inputFormat: this.format.date,
      initialValue: moment(),
      weekStart: 1,
      weekdayFormat: 'short',
      min: moment(),
      ...options
    };
    this._init();
  }
  
  _init() {
    for (let i = 0; i < this.datePickers.length; i++) {
      this.dateTarget = this.datePickers[i].querySelector(`${this.selector}-date`);
      this.dayTarget = this.datePickers[i].querySelector(`${this.selector}-day`);
      this.yearTarget = this.datePickers[i].querySelector(`${this.selector}-year`);
      const input = this.datePickers[i].querySelector('input');
      input.setAttribute('id', `datepicker-${i}`);
      const datePicker = document.getElementById(input.id);
      rome(datePicker, this.options);
      this._writeDate(moment());
      this._onChange(datePicker);
    }
  }
  
  _onChange(datePicker) {
    rome(datePicker).on('data', value => this._writeDate(moment(value, this.format.date)));
  }
  
  _writeDate(date) {
    this.dateTarget.dataset.date = moment(date).format(this.format.date);
    if (this.dayTarget) {
      this.dayTarget.innerText = moment(date).format(this.format.day);
    }
    if (this.yearTarget) {
      this.yearTarget.innerText = moment(date).format(this.format.year);
    }
  };
  
}

export default RomeDatePicker;

