import moment from 'moment';

const setTime = (el, time) => {
  el.dataset.date = moment(time).format('HH:mm');
};

export default setTime;
