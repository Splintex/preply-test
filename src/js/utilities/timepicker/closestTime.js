import moment from 'moment';

const roundMinute = (min, base) => {
  let result = 0;
  if (min === 0) return 0;
  for (let i = 0; i < 60 / base; i++) {
    if (min < i * base) {
      result = i * base;
      break;
    }
  }
  return result;
};

const closestTime = (time) => {
  let m = time.format('mm');
  const h = time.format('HH');
  m = roundMinute(m, 15);
  return moment().hour(h).minute(m);
};

export default closestTime;
