import moment from 'moment';
import rome from 'rome';
import uuidV4 from 'uuid/v4';
import setTime from './setTime';

class TimePicker {
  constructor(selector, options) {
    this.selector = selector;
    this.timepickers = document.querySelectorAll(selector);
    this.format = 'HH:mm';
    this.settings = {
      date: false,
      timeFormat: this.format,
      timeInterval: 900,
      initialValue: moment(),
      ...options
    };
    this._init();
  }
  
  _init() {
    for (let i = 0; i < this.timepickers.length; i++) {
      const time = this.timepickers[i].querySelector(`${this.selector}-value`);
      const input = this.timepickers[i].querySelector('input');
      input.setAttribute('id', uuidV4());
      const inputPicker = document.getElementById(input.id);
      rome(inputPicker, this.settings);
      rome(inputPicker).on('data', value => setTime(time, moment(value, this.format)));
      setTime(time, moment());
    }
  }
}

export default TimePicker;
