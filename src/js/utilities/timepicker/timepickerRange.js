import moment from 'moment';
import humanizeDuration from 'humanize-duration';
import rome from 'rome';
import uuidV4 from 'uuid/v4';
import setTime from './setTime';
import closestTime from './closestTime';

class RomeRangeTimePicker {
  constructor(selector, options) {
    this.selector = selector;
    this.timepickers = document.querySelectorAll(this.selector);
    this.format = 'HH:mm';
    this.settings = {
      date: false,
      timeFormat: this.format,
      timeInterval: 900,
      initialValue: moment(),
      ...options
    };
    this.now = closestTime(moment());
    this.end = this.now.clone().add(1, 'hours');
    this._init();
  }
  
  _init() {
    for (let i = 0; i < this.timepickers.length; i++) {
      const timeFrom = this.timepickers[i].querySelector(`${this.selector}-from`);
      const timeTo = this.timepickers[i].querySelector(`${this.selector}-to`);
      const inputFrom = timeFrom.querySelector('input');
      const inputTo = timeTo.querySelector('input');
      inputFrom.setAttribute('id', uuidV4());
      inputTo.setAttribute('id', uuidV4());
      this.timePickerFrom = document.getElementById(inputFrom.id);
      this.timePickerTo = document.getElementById(inputTo.id);
      this.timeDiff = this.timepickers[i].querySelector(`${this.selector}-diff`);
      
      this._initTimePicker(this.timePickerFrom, {
        initialValue: this.now,
        timeValidator: (date) => {
          const m = moment(date);
          const minTime = m.clone().hour(7).minute(59);
          const maxTime = m.clone().hour(21).minute(1);
          return m.isAfter(minTime) && m.isBefore(maxTime);
        }
      });
      
      this._initTimePicker(this.timePickerTo, {
        initialValue: this.end,
        timeValidator: (date) => {
          const m = moment(date);
          const minTime = rome(this.timePickerFrom).getDate();
          const maxTime = m.clone().hour(22).minute(1);
          return m.isAfter(minTime) && m.isBefore(maxTime);
        }
      });
      
      this._onChange(this.timePickerFrom, timeFrom);
      this._onChange(this.timePickerTo, timeTo);
      this._writeTimeInterval(this.timeDiff, this.now, this.end);
      setTime(timeFrom, closestTime(this.now));
      setTime(timeTo, closestTime(this.end));
    }
  }
  
  _initTimePicker(timePicker, settings) {
    rome(timePicker, {
      ...this.settings,
      ...settings
    });
  }
  
  _onChange(timePicker, target) {
    rome(timePicker).on('data', (value) => {
      setTime(target, moment(value, this.format));
      const from = rome(this.timePickerFrom).getDate();
      const to = rome(this.timePickerTo).getDate();
      this._writeTimeInterval(this.timeDiff, from, to);
    });
  }
  
  _writeTimeInterval(el, from, to) {
    const result = moment(to, 'ms').diff(moment(from, 'ms'));
    el.innerText = humanizeDuration(result, {
      delimiter: ' ',
      units: ['h', 'm'],
      round: true
    })
  }
}

export default RomeRangeTimePicker;