import React from 'react';
import {connect} from 'react-redux';

let LessonType = ({onChangeType}) => {
  const onChangeHandler = (event) => {
    onChangeType(event.target.value === 'online');
  };
  return (
    <div className="radio-group">
      <label className="radio">
        <span className="radio__in">
          <input type="radio" name="lessonType" onChange={onChangeHandler} value="online" defaultChecked={true}/>
          <span className="radio__text">Online lesson</span>
        </span>
      </label>
      <label className="radio">
        <span className="radio__in">
        <input type="radio" name="lessonType" onChange={onChangeHandler} value="offline"/>
        <span className="radio__text">Offline lesson</span>
      </span>
      </label>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    onChangeType: (isOnline) => {
      dispatch({
        type: 'CHANGE_LESSON_TYPE',
        online: isOnline
      })
    }
  };
};

LessonType = connect(
  null,
  mapDispatchToProps
)(LessonType);

export default LessonType;