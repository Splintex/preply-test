import React from 'react';
import {connect} from 'react-redux';
import TimePickerRange from './TimePickerRange';

const mapDispatchToProps = (dispatch) => {
  return {
    onTimeStartChange: (time) => {
      dispatch({
        type: 'CHANGE_TIME_START',
        timeStart: time
      })
    },
    onTimeEndChange: (time) => {
      dispatch({
        type: 'CHANGE_TIME_END',
        timeEnd: time
      })
    }
  };
};

const TimePickerRangeContainer = connect(
  null,
  mapDispatchToProps
)(TimePickerRange);

export default TimePickerRangeContainer;