import React from 'react';

let SelectedStudent = ({student, onClick}) => {
  if (student) {
    const avatar = student.img
      ? <img src={student.img} alt={student.name}/>
      : <i className="icon icon-send"></i>;
    return (
      <div className="select-item" onClick={onClick}>
        <span className="select-item__icon">
          <i className="icon icon-user"></i>
        </span>
        <div className="select-item__main">
          <div className="person">
            <div className="person__in">
              <div className="person__main">
                <div className="person__avatar">
                  {avatar}
                </div>
                <div className="person__name">{student.name}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
  return (
    <div className="select-item" onClick={onClick}>
      <span className="select-item__icon">
        <i className="icon icon-user"></i>
      </span>
      <div className="select-item__main">Select student</div>
    </div>
  );
};



export default SelectedStudent;