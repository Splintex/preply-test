import React from 'react';
import rome from 'rome';
import RomeRangeTimePicker from '../../utilities/timepicker/timepickerRange';

class TimePickerRange extends React.Component {
  constructor(props) {
    super(props);
    this.props = props;
  }
  
  render() {
    return (
      <div className="time js-timepicker-range">
        <div className="picker">
          <label className="picker__value js-timepicker-range-from">
            <input className="picker__input" type="text" ref={node => this.inputFrom = node}/>
          </label>
          <span className="picker__meta js-timepicker-range-diff"></span>
        </div>
        <span className="time__arrow">
          <i className="icon icon-arrow"></i>
        </span>
        <div className="picker">
          <label className="picker__value js-timepicker-range-to">
            <input className="picker__input" type="text" ref={node => this.inputTo = node}/>
          </label>
        </div>
      </div>
    );
  }
  
  handleChangeFrom(value) {
    this.props.onTimeStartChange(value);
  }
  
  handleChangeTo(value) {
    this.props.onTimeEndChange(value);
  }
  
  componentDidMount() {
    new RomeRangeTimePicker('.js-timepicker-range');
    const timePickerFrom = rome.find(this.inputFrom).associated;
    const timePickerTo = rome.find(this.inputTo).associated;
    const timeFrom = rome(timePickerFrom).getDateString('HH:mm');
    const timeTo = rome(timePickerTo).getDateString('HH:mm');
    rome(timePickerFrom).on('data', value => this.handleChangeFrom(value));
    rome(timePickerTo).on('data', value => this.handleChangeTo(value));
    this.handleChangeFrom(timeFrom);
    this.handleChangeTo(timeTo);
  }
}

export default TimePickerRange;