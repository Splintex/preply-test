import React from 'react';
import {connect} from 'react-redux';
import SelectedStudent from './SelectedStudent';
import LessonType from './LessonType';
import DatePickerContainer from './DatePickerContainer';
import TimePickerRangeContainer from './TimePickerRangeContainer';

let ViewLesson = ({lesson, changeView}) => {
  const onSubmitHandler = (event) => {
    event.preventDefault();
    console.log("------------- Schedule object -------------");
    console.log(lesson);
    console.log("-----------------------");
  };
  const onClickHandler = () => {
    changeView('students');
  };
  const {student} = lesson;
  return (
    <div className="popup is-active">
      <div className="popup__main">
        <form onSubmit={onSubmitHandler}>
          <div className="popup__head">
            <div className="popup__title">Schedule lesson</div>
            <button className="popup__close" type="button">
              <i className="icon icon-close"></i>
            </button>
          </div>
          <div className="popup__body">
            <div className="schedule">
              <div className="schedule__in">
                <div className="schedule__date">
                  <DatePickerContainer/>
                </div>
                <div className="schedule__time">
                  <TimePickerRangeContainer/>
                </div>
              </div>
            </div>
            <div className="popup__in">
              <div className="popup__row">
                <SelectedStudent student={student} onClick={onClickHandler}/>
              </div>
              <div className="popup__row">
                <LessonType/>
              </div>
            </div>
          </div>
          <div className="popup__footer">
            <button className="btn btn_fluid" type="submit" disabled={!student}>Schedule lesson</button>
          </div>
        </form>
      </div>
    </div>);
};

const mapStateToProps = (state) => {
  return {
    lesson: state.lesson
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    changeView: (view) => {
      dispatch({
        type: 'CHANGE_VIEW',
        view
      })
    }
  };
};

ViewLesson = connect(
  mapStateToProps,
  mapDispatchToProps
)(ViewLesson);

export default ViewLesson;