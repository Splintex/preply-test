import React from 'react';
import DatePicker from './DatePicker';
import {connect} from 'react-redux';

const mapDispatchToProps = (dispatch) => {
  return {
    onDateChange: (date) => {
      dispatch({
        type: 'CHANGE_DATE',
        date
      })
    }
  };
};

const DatePickerContainer = connect(
  null,
  mapDispatchToProps
)(DatePicker);

export default DatePickerContainer;