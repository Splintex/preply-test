import React from 'react';
import rome from 'rome';
import RomeDatePicker from '../../utilities/datepicker';

class DatePicker extends React.PureComponent {
  constructor(props) {
    super(props);
    this.props = props;
  }
  
  render() {
    return (
      <div className="date">
        <div className="date__icon">
          <i className="icon icon-clock"></i>
        </div>
        <div className="picker js-datepicker">
          <label className="picker__value js-datepicker-date">
            <input className="picker__input" ref={node => this.input = node} type="text"/>
          </label>
          <span className="picker__meta js-datepicker-day"></span>
        </div>
      </div>
    );
  }
  
  handleChange(value) {
    this.props.onDateChange(value);
  }
  
  componentDidMount() {
    new RomeDatePicker('.js-datepicker');
    const input = rome.find(this.input).associated;
    const initialValue = rome(input).getDateString('MMMM D');
    rome(input).on('data', value => this.handleChange(value));
    this.handleChange(initialValue);
  }
}

export default DatePicker;