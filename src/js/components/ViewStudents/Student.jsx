import React from 'react';

const Student = ({student, onSelectStudent, onChangeView}) => {
  const onClickHandler = (event) => {
    event.preventDefault();
    onSelectStudent(student);
    onChangeView('lesson');
  };
  const avatar = student.img
    ? <img src={student.img} alt={student.name}/>
    : <i className="icon icon-send"></i>;
  return (
    <li className="person">
      <a className="person__in" href="#" onClick={onClickHandler}>
        <div className="person__main">
          <div className="person__avatar">{avatar}</div>
          <div className="person__name">{student.name}</div>
        </div>
        <div className="person__info">{student.info}</div>
      </a>
    </li>
  );
};

export default Student;