import React from 'react';
import Student from './Student'

const List = ({ students, filter, onChangeView, onSelectStudent }) => {
  return (
    <div>
      <ul className="person-list">
        {students.map(student => {
          if (student.name.toLowerCase().indexOf(filter.toLowerCase()) !== -1) {
            return <Student student={student} key={student.id} onChangeView={onChangeView} onSelectStudent={onSelectStudent}/>;
          }
        })}
      </ul>
    </div>
  );
};

export default List;