import React from 'react';
import Filter from './Filter';
import ListContainer from './ListContainer';

class SelectStudent extends React.Component {
  constructor() {
    super();
    this.state = {
      filter: ''
    };
  }
  onFilterChange(event) {
    this.setState({filter: event.target.value});
  }
  render() {
    return (
      <div className="popup__tab">
        <Filter onFilterChange={this.onFilterChange.bind(this)}/>
        <div className="popup__list">
          <ListContainer filter={this.state.filter}/>
        </div>
      </div>
    );
  }
  
}

export default SelectStudent;