import React from 'react';
import {connect} from 'react-redux';
import {Tab, TabList, Tabs, TabPanel} from 'react-context-tabs'

import SelectStudent from './SelectStudent';
import AddNewContainer from './AddNewContainer';

let ViewStudents = ({changeView}) => {
  const onClickHandler = (event) => {
    changeView('lesson');
  };
  return (
    <div className="popup is-active">
      <div className="popup__main">
        <Tabs defaultTabId='select'>
          <div className="popup__head">
            <button className="btn-prev" type="button" onClick={onClickHandler}></button>
            <TabList className="tab">
              <Tab tabId='select' className="tab__item">Select student</Tab>
              <Tab tabId='add' className="tab__item">Add new student</Tab>
            </TabList>
          </div>
          <TabPanel tabId='select'>
            <SelectStudent/>
          </TabPanel>
          <TabPanel tabId='add'>
            <div className="popup__tab">
              <AddNewContainer/>
            </div>
          </TabPanel>
        </Tabs>
      </div>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    changeView: (view) => {
      dispatch({
        type: 'CHANGE_VIEW',
        view
      })
    }
  };
};
ViewStudents = connect(
  null,
  mapDispatchToProps
)(ViewStudents);

export default ViewStudents;