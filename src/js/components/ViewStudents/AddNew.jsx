import React from 'react';

const AddNew = ({onAddClick}) => {
  let inputName = null;
  let inputEmail = null;
  let btn = null;
  
  const onSubmitHandler = (event) => {
    event.preventDefault();
    const newStudent = {
      name: inputName.value,
      email: inputEmail.value
    };
    if (newStudent.name) {
      onAddClick(newStudent);
      inputName.value = '';
      inputEmail.value = '';
    }
  };
  
  const onNameChange = (event) => {
    event.target.value ? btn.removeAttribute('disabled') : btn.setAttribute('disabled', 'disabled');
  };
  
  return (
    <form className="form" onSubmit={onSubmitHandler}>
      <div className="form__text">On Preply, you can invite new students and schedule lessons with them for free. </div>
      <label className="field field_required">
        <span className="field__title">Student’s name</span>
        <input
          className="input"
          type="text"
          onChange={onNameChange}
          autoFocus={true}
          ref={node => inputName = node}/>
      </label>
      <div className="form__text">If you wish for the student to get notified about
        scheduled lessons and changes, type in their email:</div>
      <label className="field">
        <span className="field__title">Student’s email</span>
        <input
          className="input"
          type="email"
          ref={node => inputEmail = node}/>
      </label>
      <div className="form__action">
        <button
          className="btn btn_fluid"
          type="submit"
          ref={node => btn = node}
          disabled>Add new student</button>
      </div>
    </form>
  );
};

export default AddNew;