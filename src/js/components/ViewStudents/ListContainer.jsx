import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import List from './List';
// import changeView from '../../actions/changeView';
//import selectStudent from '../../actions/selectStudent';

const changeView = (view) => {
  return {
    type: 'CHANGE_VIEW',
    view
  }
};

const mapStateToProps = (state, ownProps) => {
  return {
    students: state.students,
    filter: ownProps.filter,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onChangeView: (view) => {
      dispatch({
        type: 'CHANGE_VIEW',
        view
      });
    },
    onSelectStudent: (obj) => {
      dispatch({
        type: 'SELECT_STUDENT',
        obj
      });
    }
  };
};

// const mapDispatchToProps = (dispatch) => {
//   return {
//     onSelectStudent: {
//       changeView: bindActionCreators(changeView, dispatch),
//       selectStudent: bindActionCreators(selectStudent, dispatch)
//     }
//   };
// }

const ListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(List);

export default ListContainer;