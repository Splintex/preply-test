import React from 'react';

const Filter = ({onFilterChange}) => {
  return (
    <div>
      <label className="field field_search">
        <input className="input" onChange={onFilterChange} type="text" name="search" placeholder="Search students" autoFocus={true}/>
        <i className="icon icon-search"></i>
      </label>
    </div>
  );
};

export default Filter;