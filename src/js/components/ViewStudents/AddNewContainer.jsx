import React from 'react';
import {connect} from 'react-redux';
import AddNew from './AddNew';

const mapDispatchToProps = (dispatch) => {
  return {
    onAddClick: (obj) => {
      dispatch({
        type: 'ADD_NEW_STUDENT',
        obj
      })
    }
  };
};

const AddNewContainer = connect(
  null,
  mapDispatchToProps
)(AddNew);

export default AddNewContainer;