import React from 'react';
import {connect} from 'react-redux';
import ViewLesson from './ViewLesson';
import ViewStudents from './ViewStudents';

let Popup = ({view}) => {
  switch (view) {
    case 'lesson':
      return <ViewLesson/>;
    case 'students':
      return <ViewStudents/>;
    default:
      return <ViewLesson/>;
  }
};

const mapStateToProps = (state) => {
  return {
    view: state.view
  };
};

Popup = connect(
  mapStateToProps,
)(Popup);


export default Popup;
