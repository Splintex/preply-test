import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import configStore from './configStore';
import Popup from './components/Popup';

ReactDOM.render(
  <Provider store={configStore()}>
    <Popup/>
  </Provider>,
  document.getElementById('app')
);