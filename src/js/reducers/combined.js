import view from './view';
import lesson from './lesson';
import students from './students';
import {combineReducers} from 'redux';

export default combineReducers({
  view,
  lesson,
  students
});

