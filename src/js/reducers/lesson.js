import selectStudent from '../actions/selectStudent';
import changeLessonType from '../actions/changeLessonType';

const lesson = (state = {}, action) => {
  switch (action.type) {
    case 'SELECT_STUDENT':
      return selectStudent(state, action);
    case 'CHANGE_LESSON_TYPE':
      return changeLessonType(state);
    case 'CHANGE_DATE':
      return {
        ...state,
        info: {
          ...state.info,
          date: action.date
        }
      };
    case 'CHANGE_TIME_START':
      return {
        ...state,
        info: {
          ...state.info,
          timeStart: action.timeStart
        }
      };
    case 'CHANGE_TIME_END':
      return {
        ...state,
        info: {
          ...state.info,
          timeEnd: action.timeEnd
        }
      };
    default:
      return state;
  }
};

export default lesson;