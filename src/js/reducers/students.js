import addNewStudent from '../actions/addNewStudent';

const students = (state = [], action) => {
  switch (action.type) {
    case 'ADD_NEW_STUDENT':
      return addNewStudent(state, action);
    default:
      return state;
  }
};

export default students;