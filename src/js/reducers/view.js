import changeView from '../actions/changeView';

const view = (state = '', action) => {
  switch (action.type) {
    case 'CHANGE_VIEW':
      return changeView(action);
    default:
      return state;
  }
};

export default view;